package cn.org.fluent.mybatis.many2many.demo.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.LogicDelete;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.IEntity;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * MemberFavoriteEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "t_member_favorite",
    schema = "fluent_mybatis"
)
public class MemberFavoriteEntity extends RichEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId("id")
    private Long id;

    /**
     * 更新时间
     */
    @TableField(
        value = "gmt_modified",
        insert = "now()",
        update = "now()"
    )
    private Date gmtModified;

    /**
     * 是否逻辑删除
     */
    @TableField(
        value = "is_deleted",
        insert = "0"
    )
    @LogicDelete
    private Boolean isDeleted;

    /**
     * 爱好: 电影, 爬山, 徒步...
     */
    @TableField("favorite")
    private String favorite;

    /**
     * 创建时间
     */
    @TableField("gmt_created")
    private Date gmtCreated;

    /**
     * member表外键
     */
    @TableField("member_id")
    private Long memberId;

    @Override
    public final Class<? extends IEntity> entityClass() {
        return MemberFavoriteEntity.class;
    }
}
