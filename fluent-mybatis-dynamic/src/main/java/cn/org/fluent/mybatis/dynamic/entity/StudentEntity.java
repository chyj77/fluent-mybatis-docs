package cn.org.fluent.mybatis.dynamic.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.LogicDelete;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * StudentEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "student",
    schema = "fluent_mybatis"
)
public class StudentEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /**
   * 编号
   */
  @TableId("id")
  private Long id;

  /**
   * 存入数据库的时间
   */
  @TableField(
      value = "gmt_created",
      insert = "now()"
  )
  private Date gmtCreated;

  /**
   * 修改的时间
   */
  @TableField(
      value = "gmt_modified",
      insert = "now()",
      update = "now()"
  )
  private Date gmtModified;

  /**
   */
  @TableField(
      value = "is_deleted",
      insert = "0"
  )
  @LogicDelete
  private Boolean isDeleted;

  /**
   * 邮箱
   */
  @TableField("email")
  private String email;

  /**
   * 性别
   */
  @TableField("gender")
  private Integer gender;

  /**
   * 状态(0:正常,1:锁定)
   */
  @TableField("locked")
  private Integer locked;

  /**
   * 姓名
   */
  @TableField("name")
  private String name;

  /**
   * 电话
   */
  @TableField("phone")
  private String phone;

  @Override
  public final Class entityClass() {
    return StudentEntity.class;
  }
}
