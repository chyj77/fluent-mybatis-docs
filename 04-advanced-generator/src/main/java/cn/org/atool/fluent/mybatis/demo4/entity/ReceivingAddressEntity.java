package cn.org.atool.fluent.mybatis.demo4.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.LogicDelete;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * ReceivingAddressEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "receiving_address",
    schema = "fluent_mybatis"
)
public class ReceivingAddressEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /**
   * 主键id
   */
  @TableId("id")
  private Long id;

  /**
   * 更新时间
   */
  @TableField(
      value = "gmt_modified",
      insert = "now()",
      update = "now()"
  )
  private Date gmtModified;

  /**
   * 是否逻辑删除
   */
  @TableField(
      value = "is_deleted",
      insert = "0"
  )
  @LogicDelete
  private Boolean isDeleted;

  /**
   * 城市
   */
  @TableField("city")
  private String city;

  /**
   * 详细住址
   */
  @TableField("detail_address")
  private String detailAddress;

  /**
   * 区
   */
  @TableField("district")
  private String district;

  /**
   * 创建时间
   */
  @TableField("gmt_create")
  private Date gmtCreate;

  /**
   * 省份
   */
  @TableField("province")
  private String province;

  /**
   * 用户id
   */
  @TableField("user_id")
  private Long userId;

  @Override
  public final Class entityClass() {
    return ReceivingAddressEntity.class;
  }
}
