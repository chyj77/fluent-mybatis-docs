package cn.org.atool.fluent.mybatis.demo4.mix;

import cn.org.atool.fluent.mybatis.demo4.dm.HelloWorldDataMap;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.spec.IMix;
import org.test4j.module.spec.annotations.Step;

/**
 * 数据库[hello_world]表数据准备和校验通用方法
 *
 * @author Powered By Test4J
 */
public class HelloWorldTableMix implements IMix {
  @Step("清空表[hello_world]数据")
  public HelloWorldTableMix cleanHelloWorldTable() {
    db.table("hello_world").clean();
    return this;
  }

  @Step("准备表[hello_world]数据{1}")
  public HelloWorldTableMix readyHelloWorldTable(HelloWorldDataMap data) {
    db.table("hello_world").insert(data);
    return this;
  }

  @Step("验证表[hello_world]有全表数据{1}")
  public HelloWorldTableMix checkHelloWorldTable(HelloWorldDataMap data, EqMode... modes) {
    db.table("hello_world").query().eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[hello_world]有符合条件{1}的数据{2}")
  public HelloWorldTableMix checkHelloWorldTable(String where, HelloWorldDataMap data,
      EqMode... modes) {
    db.table("hello_world").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[hello_world]有符合条件{1}的数据{2}")
  public HelloWorldTableMix checkHelloWorldTable(HelloWorldDataMap where, HelloWorldDataMap data,
      EqMode... modes) {
    db.table("hello_world").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[hello_world]有{1}条符合条件{2}的数据")
  public HelloWorldTableMix countHelloWorldTable(int count, HelloWorldDataMap where) {
    db.table("hello_world").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[hello_world]有{1}条符合条件{2}的数据")
  public HelloWorldTableMix countHelloWorldTable(int count, String where) {
    db.table("hello_world").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[hello_world]有{1}条数据")
  public HelloWorldTableMix countHelloWorldTable(int count) {
    db.table("hello_world").query().sizeEq(count);
    return this;
  }
}
