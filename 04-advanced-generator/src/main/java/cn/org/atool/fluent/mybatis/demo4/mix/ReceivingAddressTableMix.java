package cn.org.atool.fluent.mybatis.demo4.mix;

import cn.org.atool.fluent.mybatis.demo4.dm.ReceivingAddressDataMap;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.spec.IMix;
import org.test4j.module.spec.annotations.Step;

/**
 * 数据库[receiving_address]表数据准备和校验通用方法
 *
 * @author Powered By Test4J
 */
public class ReceivingAddressTableMix implements IMix {
  @Step("清空表[receiving_address]数据")
  public ReceivingAddressTableMix cleanReceivingAddressTable() {
    db.table("receiving_address").clean();
    return this;
  }

  @Step("准备表[receiving_address]数据{1}")
  public ReceivingAddressTableMix readyReceivingAddressTable(ReceivingAddressDataMap data) {
    db.table("receiving_address").insert(data);
    return this;
  }

  @Step("验证表[receiving_address]有全表数据{1}")
  public ReceivingAddressTableMix checkReceivingAddressTable(ReceivingAddressDataMap data,
      EqMode... modes) {
    db.table("receiving_address").query().eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[receiving_address]有符合条件{1}的数据{2}")
  public ReceivingAddressTableMix checkReceivingAddressTable(String where,
      ReceivingAddressDataMap data, EqMode... modes) {
    db.table("receiving_address").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[receiving_address]有符合条件{1}的数据{2}")
  public ReceivingAddressTableMix checkReceivingAddressTable(ReceivingAddressDataMap where,
      ReceivingAddressDataMap data, EqMode... modes) {
    db.table("receiving_address").queryWhere(where).eqDataMap(data, modes);
    return this;
  }

  @Step("验证表[receiving_address]有{1}条符合条件{2}的数据")
  public ReceivingAddressTableMix countReceivingAddressTable(int count,
      ReceivingAddressDataMap where) {
    db.table("receiving_address").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[receiving_address]有{1}条符合条件{2}的数据")
  public ReceivingAddressTableMix countReceivingAddressTable(int count, String where) {
    db.table("receiving_address").queryWhere(where).sizeEq(count);
    return this;
  }

  @Step("验证表[receiving_address]有{1}条数据")
  public ReceivingAddressTableMix countReceivingAddressTable(int count) {
    db.table("receiving_address").query().sizeEq(count);
    return this;
  }
}
