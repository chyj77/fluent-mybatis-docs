package cn.org.atool.fluent.mybatis.demo4.dm;

import java.util.Date;
import java.util.function.Consumer;
import java.util.function.Supplier;
import org.test4j.hamcrest.matcher.modes.EqMode;
import org.test4j.module.database.IDatabase;
import org.test4j.module.database.annotations.ColumnDef;
import org.test4j.module.database.annotations.ScriptTable;
import org.test4j.tools.datagen.DataMap;
import org.test4j.tools.datagen.IDataMap;
import org.test4j.tools.datagen.KeyValue;

/**
 * HelloWorldDataMap: 表(实体)数据对比(插入)构造器
 *
 * @author Powered By Test4J
 */
@ScriptTable("hello_world")
@SuppressWarnings({"unused", "rawtypes"})
public class HelloWorldDataMap extends DataMap<HelloWorldDataMap> {
  private boolean isTable;

  private final Supplier<Boolean> supplier = () -> this.isTable;

  @ColumnDef(
      value = "id",
      type = "BIGINT UNSIGNED",
      primary = true,
      autoIncrease = true,
      notNull = true
  )
  public final transient KeyValue<HelloWorldDataMap> id = new KeyValue<>(this, "id", "id", supplier);

  @ColumnDef(
      value = "gmt_created",
      type = "DATETIME"
  )
  public final transient KeyValue<HelloWorldDataMap> gmtCreated = new KeyValue<>(this, "gmt_created", "gmtCreated", supplier);

  @ColumnDef(
      value = "gmt_modified",
      type = "DATETIME"
  )
  public final transient KeyValue<HelloWorldDataMap> gmtModified = new KeyValue<>(this, "gmt_modified", "gmtModified", supplier);

  @ColumnDef(
      value = "is_deleted",
      type = "TINYINT",
      defaultValue = "0"
  )
  public final transient KeyValue<HelloWorldDataMap> isDeleted = new KeyValue<>(this, "is_deleted", "isDeleted", supplier);

  @ColumnDef(
      value = "say_hello",
      type = "VARCHAR(100)"
  )
  public final transient KeyValue<HelloWorldDataMap> sayHello = new KeyValue<>(this, "say_hello", "sayHello", supplier);

  @ColumnDef(
      value = "your_name",
      type = "VARCHAR(100)"
  )
  public final transient KeyValue<HelloWorldDataMap> yourName = new KeyValue<>(this, "your_name", "yourName", supplier);

  HelloWorldDataMap(boolean isTable) {
    super();
    this.isTable = isTable;
  }

  HelloWorldDataMap(boolean isTable, int size) {
    super(size);
    this.isTable = isTable;
  }

  /**
   * 创建HelloWorldDataMap
   * 初始化主键和gmtCreate, gmtModified, isDeleted等特殊值
   */
  public HelloWorldDataMap init() {
    this.id.autoIncrease();
    this.gmtCreated.values(new Date());
    this.gmtModified.values(new Date());
    this.isDeleted.values(false);
    return this;
  }

  public HelloWorldDataMap with(Consumer<HelloWorldDataMap> init) {
    init.accept(this);
    return this;
  }

  public static HelloWorldDataMap table() {
    return new HelloWorldDataMap(true, 1);
  }

  public static HelloWorldDataMap table(int size) {
    return new HelloWorldDataMap(true, size);
  }

  public static HelloWorldDataMap entity() {
    return new HelloWorldDataMap(false, 1);
  }

  public static HelloWorldDataMap entity(int size) {
    return new HelloWorldDataMap(false, size);
  }

  /**
   * DataMap数据和表[hello_world]数据比较
   */
  public HelloWorldDataMap eqTable(EqMode... modes) {
    IDatabase.db.table("hello_world").query().eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[hello_world]数据比较
   */
  public HelloWorldDataMap eqQuery(String query, EqMode... modes) {
    IDatabase.db.table("hello_world").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * DataMap数据和表[hello_world]数据比较
   */
  public HelloWorldDataMap eqQuery(IDataMap query, EqMode... modes) {
    IDatabase.db.table("hello_world").queryWhere(query).eqDataMap(this, modes);
    return this;
  }

  /**
   * 清空[hello_world]表数据
   */
  public HelloWorldDataMap clean() {
    IDatabase.db.cleanTable("hello_world");
    return this;
  }

  /**
   * 插入[hello_world]表数据
   */
  public HelloWorldDataMap insert() {
    IDatabase.db.table("hello_world").insert(this);
    return this;
  }

  /**
   * 先清空, 再插入[hello_world]表数据
   */
  public HelloWorldDataMap cleanAndInsert() {
    return this.clean().insert();
  }

  public static class Factory {
    public HelloWorldDataMap table() {
      return HelloWorldDataMap.table();
    }

    public HelloWorldDataMap table(int size) {
      return  HelloWorldDataMap.table(size);
    }

    public HelloWorldDataMap initTable() {
      return HelloWorldDataMap.table().init();
    }

    public HelloWorldDataMap initTable(int size) {
      return  HelloWorldDataMap.table(size).init();
    }

    public HelloWorldDataMap entity() {
      return HelloWorldDataMap.entity();
    }

    public HelloWorldDataMap entity(int size) {
      return  HelloWorldDataMap.entity(size);
    }
  }
}
